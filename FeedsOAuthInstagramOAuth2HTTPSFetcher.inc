<?php

/**
 * @file
 * Definition of the import batch object created on the fetching stage by
 * OAuth2HTTPSFetcher.
 */

class FeedsOAuthInstagramOAuthHTTPFetcherResult extends OAuthHTTPFetcherResult {
  //Cache result of method getRaw() since it's called multiple times
  protected $memoized_raw;
  /**
   * Implementation of FeedsImportBatch::getRaw().
   */
  public function setRaw($raw) {
    $this->memoized_raw = $raw;
  }

  public function getRaw() {
    if(!isset($this->memoized_raw)) {
      $this->memoized_raw = parent::getRaw();
    }
    return $this->memoized_raw;
  }
}

/**
 * Support OAuth 2.0 authentication.
 */
class FeedsOAuthInstagramOAuth2HTTPSFetcher extends OAuth2HTTPSFetcher {
  /**
   * Declare defaults.
   */
  public function configDefaults() {
    return array('scope' => 'basic') + parent::configDefaults() + array(
      'max_id_parameter_name' => 'next_max_id',
    );
  }

  public function sourceDefaults() {
    return parent::sourceDefaults() + array(
      'max_id_parameter_name' => 'max_id',
    );
  }
  /**
   * Add form options.
   */
  //TODO: reset max_id if changing url
  public function sourceForm($source_config) {
    $form = parent::sourceForm($source_config);
    //TODO: add count parameter? Will fuck up results, reset state if changed?
    $form['max_id_parameter_name'] = array(
      '#type' => 'textfield',
      '#title' => t('Next max id parameter name'),
      '#description' => t('Name of the "max id" parameter in the request url.'),
      '#default_value' => !empty($source_config['max_id_parameter_name']) ? $source_config['max_id_parameter_name'] : 'max_id',
    );
    return $form;
  }

  /**
   * Use signed URL to fetch content.
   */
  public function fetch(FeedsSource $source) {
    //rename to current_next_max_id for clarity?
    list($next_max_id, $current_max_id) = feeds_oauth_instagram_get_state($source->id, $source->feed_nid);
    $source_config = $source->getConfigFor($this);

    //TODO: handle appending of get query parameter in better/safer way
    //Parse url and than piece back together?
    $url = trim($source_config['source']);
    
    // Returns a string if the URL has parameters or NULL if not

    if($next_max_id) {
      $query = parse_url($url, PHP_URL_QUERY);
      if($query) {
        $url .= '&';
      }
      else {
        $url .= '?';
      }
      //TODO: config for this
      $url .= $source_config['max_id_parameter_name'] . '=' . $next_max_id;
    }
    $result = new FeedsOAuthInstagramOAuthHTTPFetcherResult(
      $url,
      $this->config['authenticator'],
      $this->config['consumer_key'],
      $this->config['consumer_secret'],
      $this->id,
      $this->config['site_id'],
      $this->config['method'],
      $this->getAuthenticatedUser($source),
      $this->two
    );

    $raw = $result->getRaw();

    $json = drupal_json_decode($raw);
    //Parse raw for next_max_id
    $state = $source->state(FEEDS_FETCH);
    $state->feeds_oauth_instagram = new stdClass();
    $envelope_next_max_id_name ='next_' . $source_config['max_id_parameter_name'];
    $state->feeds_oauth_instagram->next_max_id = 
      isset($json['pagination'][$envelope_next_max_id_name]) ?
      $json['pagination'][$envelope_next_max_id_name] : $next_max_id;
    $state->feeds_oauth_instagram->current_max_id = $next_max_id;

    $context = array(
      'source_id' => $source->id,
      'feed_nid' => $source->feed_nid,
    );

    drupal_alter('feeds_oauth_instagram_data', $json['data'], $context);

    $raw = drupal_json_encode($json);
    $result->setRaw($raw);

    return $result;
  }


  /**
   * Clear all caches for results for given source.
   *
   * @param FeedsSource $source
   *   Source information for this expiry. Implementers can choose to only clear
   *   caches pertaining to this source.
   */
  public function clear(FeedsSource $source) {
    db_delete('feeds_oauth_instagram_state')
      ->condition('importer_id', $source->id)
      ->condition('feed_nid', $source->feed_nid)
      ->execute();
    parent::clear($source);
  }
}
